module Sound.Sc3.Pim.Mix where

import qualified Sound.File.HSndFile as F {- hsc3-sf-hsndfile -}

import Sound.Sc3.Pim.Envelope {- after-pim -}

type Time = Double
type Amplitude = Double
type Stereo_Location = Double

data Mix = Mix
  { file_name :: String
  , start_time :: Time
  , inskip_time :: Time
  , duration :: Time
  , increment :: Double
  , amplitude_f :: Envelope_C Time Amplitude
  , stereo_location_f :: Envelope_C Time Stereo_Location
  }

{-
data Mix' = Mix'
  { mix_fname :: FilePath
  , mix_nc :: Int
  , mix_inskip :: Time
  , mix_duration :: Time
  , mix_increment :: Double
  , amplitude_f :: Envelope Amplitude
  , stereo_location_f :: Envelope Stereo_Location
  , mix_buf_frames :: Int
  }
-}
