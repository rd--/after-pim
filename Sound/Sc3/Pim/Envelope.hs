-- | Break-point envelopes
module Sound.Sc3.Pim.Envelope where

import Data.List.Split {- split -}

import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Plot as P {- hsc3-plot -}

-- | Envelope given as a set /(0,1)/ constrained /(x,y)/ coordinates.
type Envelope_C t e = [(t, e)]

{- | Transform sequence of /x/ then /y/ values to 'Envelope_C'.

>>> to_env [0,0.2,0.17,0.9,0.31,0.2,0.64,0.2,0.76,0.9,1,0.2]
[(0.0,0.2),(0.17,0.9),(0.31,0.2),(0.64,0.2),(0.76,0.9),(1.0,0.2)]
-}
to_env :: [a] -> Envelope_C a a
to_env =
  let f x = case x of
        [i, j] -> (i, j)
        _ -> undefined
  in map f . chunksOf 2

{- | Inverse of 'to_env'.

>>> from_env (to_env [0,0.5,1,0.5])
[0.0,0.5,1.0,0.5]
-}
from_env :: Envelope_C a a -> [a]
from_env =
  let f (i, j) = [i, j]
  in concatMap f

{- | Linear interpolation.

>>> map (interpolate_linear 5 7) [0,0.25,0.5,1]
[5.0,5.5,6.0,7.0]
-}
interpolate_linear :: Num a => a -> a -> a -> a
interpolate_linear l r i = (i * (r - l)) + l

{- | Lookup /y/ value at indicated /x/.

>>> env_lookup (to_env [0,0,1/4,1/2,1,1]) 0.5 == 2/3
True
-}
env_lookup :: (Ord t, Fractional t, Show t) => Envelope_C t t -> t -> t
env_lookup e n =
  case e of
    ((x0, y0) : (x1, y1) : r) ->
      if n >= x0 && n <= x1
        then interpolate_linear y0 y1 ((n - x0) / (x1 - x0))
        else env_lookup ((x1, y1) : r) n
    _ -> error ("env_lookup: " ++ show (e, n))

-- | Generate 'Envelope' from 'Envelope_C'.
to_envelope' :: Num a => Envelope_C a a -> Envelope a
to_envelope' e = envCoord e 1 1 EnvLin

-- | Type specialised 'to_envelope''.
to_envelope :: Real a => Envelope_C a a -> Envelope Ugen
to_envelope e =
  let f (i, j) = (constant i, constant j)
  in envCoord (map f e) 1 1 EnvLin

{- | 'P.plotEnvelope' of 'to_envelope''.

> plotEnvelope_C [to_env [0,0,1/4,1/2,1,1]]
-}
plotEnvelope_C :: (P.PNum t, Ord t, Floating t, Enum t) => [Envelope_C t t] -> IO ()
plotEnvelope_C = P.plotEnvelope . map to_envelope'
