module Sound.Sc3.Pim.Cycle where

import Data.List {- base -}
import System.FilePath {- filepath -}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Lang.Pattern as P {- hsc3-lang -}

import Sound.Sc3.Pim.Envelope
import Sound.Sc3.Pim.Sf.Db

{-
import qualified System.FilePath.Glob as G {- glob -}

glob :: String -> FilePath -> IO [FilePath]
glob p = fmap (head.fst) . G.globDir [G.compile p]

sf_names_glob :: FilePath -> String -> String -> IO [FilePath]
sf_names_glob d a p = glob p (d </> a)
-}

{- | Sound file names

>>> sf_names_s "wav" Nothing "d" "a" "c" ["1","2"]
["d/a/c1.wav","d/a/c2.wav"]
-}
sf_names_s :: String -> Maybe Int -> FilePath -> String -> String -> [String] -> [FilePath]
sf_names_s t p d a c =
  let g n =
        let m = length n
        in case p of
            Just p' ->
              if m < p'
                then replicate (p' - m) '0' ++ n
                else n
            Nothing -> n
      f i = d </> a </> c ++ g i <.> t
  in map f

{- | Sound file names

>>> sf_names "wav" Nothing "d" "a" "c" [1,2]
["d/a/c1.wav","d/a/c2.wav"]
-}
sf_names :: Show t => String -> Maybe Int -> FilePath -> String -> String -> [t] -> [FilePath]
sf_names t p d a c n = sf_names_s t p d a c (map show n)

-- * Cycle

type R = Double

-- | Note that the 'stereo_location_f' is @(0,1)@, not @(-1,1)@.
data Cycle = Cycle
  { sound_files :: [FilePath]
  -- ^ Sequence of sound files.
  , random_file_select :: Bool
  , cycle_length :: Int
  , tempo_c :: R
  , tempo_f :: Envelope_C R R
  , amplitude_c :: R
  , amplitude_f :: Envelope_C R R
  , stereo_location_f :: Envelope_C R R
  , exists_f :: Envelope_C R R
  , exists_set :: Maybe [Int]
  , increment_c :: R
  , increment_f :: Envelope_C R R
  , increment_q :: R -> R
  -- ^ Increment post-processor (q=quantise).
  }

{-
cycle :: Cycle
cycle =
    let u = [(0.0,1.0),(1.0,1.0)]
    in Cycle [] 0 60 u 1 u [(0.0,0.5),(1.0,0.5)] u Nothing
-}

cyc_env_set :: Cycle -> [Envelope_C R R]
cyc_env_set c = [tempo_f c, amplitude_f c, stereo_location_f c, exists_f c, increment_f c]

cyc_plot :: Cycle -> IO ()
cyc_plot = plotEnvelope_C . cyc_env_set

cyc_set_plot :: [Cycle] -> IO ()
cyc_set_plot = plotEnvelope_C . concatMap cyc_env_set

cyc_index :: Cycle -> Int -> R
cyc_index c n =
  let fi = fromIntegral
  in fi n / fi (cycle_length c)

cyc_lookup_f :: (Cycle -> Envelope_C R R) -> Cycle -> Int -> R
cyc_lookup_f f c = env_lookup (f c) . cyc_index c

{-
cyc_amplitude :: Cycle -> Int -> R
cyc_amplitude c = cyc_lookup_f amplitude_f c
-}

tempo_to_duration :: R -> R
tempo_to_duration t = 60 / t

cyc_at :: Cycle -> Int -> (R, R, R, R, R)
cyc_at c n =
  let f g = cyc_lookup_f g c n
      i = increment_q c (increment_c c * f increment_f)
      t = tempo_c c * f tempo_f
      a = amplitude_c c * f amplitude_f
      e =
        let e' = f exists_f
        in case exists_set c of
            Nothing -> e'
            Just s -> if n `elem` s then e' else 0
      l = f stereo_location_f
  in (t, a, e, l, i)

-- * Sc3

cyc_sf_db :: Int -> [Cycle] -> Sf_Db
cyc_sf_db i c = sf_db_build i (concatMap sound_files c)

cyc_ugen :: Int -> Ugen
cyc_ugen nc =
  let a = control kr "amp" 0.1
      b = control kr "buf" 0.0
      l = control kr "loc" 0.0
      i = control kr "incr" 1.0
      o = playBuf nc ar b (bufRateScale kr b * i) 1 0 NoLoop RemoveSynth
  in out 0 (pan2 o (linLin l 0 1 (-1) 1) a)

cyc_synthdef :: Int -> Synthdef
cyc_synthdef = synthdef "cyc" . cyc_ugen

cyc_pattern :: Sf_Db -> Cycle -> P.P P.Event
cyc_pattern db c =
  let b = map (fromIntegral . sf_db_index db) (sound_files c)
      b' =
        if random_file_select c
          then P.prand 'α' b P.inf
          else P.pseq b P.inf
      (t, a, e, l, i) = unzip5 (map (cyc_at c) [0 .. cycle_length c - 1])
      e' = P.pzipWith (>) (P.toP e) (P.pwhite 'β' 0.0 1.0 P.inf)
      fromR = P.toP . fmap P.F_Double
  in P.pbind
      [ (P.K_instr, P.pinstr' (P.Instr_Ref "cyc" False))
      , (P.K_dur, fmap (60 /) (fromR t))
      , (P.K_param "buf", b')
      , (P.K_amp, fromR a)
      , (P.K_param "incr", fromR i)
      , (P.K_rest, P.pif e' 0 1) -- rest for non-existing notes
      , (P.K_param "loc", fromR l)
      ]

cyc_patterns :: Sf_Db -> [Cycle] -> P.P P.Event
cyc_patterns db = P.ppar . map (cyc_pattern db)

-- | Load 'Sf_Db' to @scsynth@ and send 'cyc_synthdef'.
cyc_init :: Sf_Db -> IO Message
cyc_init db = withSc3 (sf_db_load db >> async (d_recv (cyc_synthdef 1)))
