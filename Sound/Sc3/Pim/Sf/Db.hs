module Sound.Sc3.Pim.Sf.Db where

import qualified Data.HashMap.Lazy as HashMap {- unordered-containers -}
import Data.List {- base -}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

-- | Map from 'FilePath' to 'Buffer_Id'.
type Sf_Db = HashMap.HashMap FilePath Buffer_Id

-- | Build 'Sf_Db' sequentially from initial 'Buffer_Id'.
sf_db_build :: Buffer_Id -> [FilePath] -> Sf_Db
sf_db_build i f = HashMap.fromList (zip (nub f) [i ..])

-- | Synonym for 'HashMap.!'
sf_db_index :: Sf_Db -> FilePath -> Int
sf_db_index = (HashMap.!)

-- | 'b_allocRead' of 'Sf_Db'.
sf_db_load :: Transport m => Sf_Db -> m ()
sf_db_load =
  let f (nm, i) = b_allocRead i nm 0 0
  in mapM_ (async . f) . HashMap.toList
