all:
	echo "after-pim"

mk-cmd:
	echo "after-pim - NIL"

clean:
	rm -fR dist

push-all:
	r.gitlab-push.sh after-pim

indent:
	fourmolu -i Sound

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
