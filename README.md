after-pim
---------

[hsc3](http://rohandrape.net/?t=hsc3) re-implementations of
[Pim](http://rohandrape.net/?t=rfpce) generators

© [rohan drape](http://rohandrape.net/),
  1998-2024,
  [gpl](http://gnu.org/copyleft/)

* * *

```
$ doctest Sound
Examples: 6  Tried: 6  Errors: 0  Failures: 0
$
```
